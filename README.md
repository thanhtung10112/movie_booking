# Movie_Booking

## Requirements:

    npm:  you can install `npm` by running `npm i -g npm`

## Run the project up in dev mode

```
npm i (node_modules)
npm start
```

## Features:

- Filter for better search movie and ticket, comment on the movie detail page
- Login, register, and book tickets
- Admin features: CRUD users, movies
- Responsive mobile and desktop
- Implement a Progressive Web App with Create React App

## Technologies:

React Hooks, Material-UI, Redux, Redux-thunk, Axios, Bootstrap 4(Css)

## live demo

- You can view a live demo over at [Vercel deployment platform](https://movie-booking-project.vercel.app/).
- Username: admintest, password: admintest
