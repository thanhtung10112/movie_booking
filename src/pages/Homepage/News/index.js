import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import Fade from "@material-ui/core/Fade";

import useStyles from "./style";
import Seperate from "../../../components/Seperate";

function TabPanel(props) {
  const { isMobile, children, value, index, ...other } = props;
  return (
    <div hidden={value !== index} {...other}>
      <Box p={isMobile && index === 0 ? 1 : 3}>{children}</Box>
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root} id="tintuc">
      <Seperate />
      <div className={classes.content}>
        <AppBar className={classes.appBar} position="static">
          <Tabs centered value={value} onChange={handleChange}>
            <Tab
              disableRipple
              classes={{
                root: classes.tabButton,
                selected: classes.tabSelected,
              }}
              label="Điện Ảnh 24h"
            />
            <Tab
              disableRipple
              classes={{
                root: classes.tabButton,
                selected: classes.tabSelected,
              }}
              label="Review"
            />
            <Tab
              disableRipple
              classes={{
                root: classes.tabButton,
                selected: classes.tabSelected,
              }}
              label="Khuyến Mãi"
            />
          </Tabs>
        </AppBar>
        <Fade timeout={400} in={value === 0}>
          <TabPanel value={value} index={0}>
            <div className="row">
              <div className={classes.repons}>
                <a
                  href="https://vnexpress.net/the-witcher-2-hau-due-tho-san-quai-vat-4405581.html"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://i1-giaitri.vnecdn.net/2021/12/20/thewitcher2top-1639972662-9712-1639974315.jpg?w=300&h=180&q=100&dpr=1&fit=crop&s=fKgPq3V9KnpEqZh7LvM-qw"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      ‘The Witcher 2’: Hậu duệ thợ săn quái vật
                    </h4>
                    <p className="text-secondary">
                      Geralt - tài tử Henry Cavill đóng - huấn luyện công chúa
                      Ciri trở thành thợ săn quái vật trong “The Witcher” mùa
                      hai.
                    </p>
                  </div>
                </a>
              </div>
              <div className={classes.repons}>
                <a
                  href="https://vnexpress.net/spider-man-3-khuynh-dao-phong-ve-viet-4405546.html"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://i1-giaitri.vnecdn.net/2021/12/20/nguoinhen1-1640001495-9961-1640001665.jpg?w=300&h=180&q=100&dpr=1&fit=crop&s=6zzRBCHW4orNh4kyiAldJQ"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      'Spider-Man 3' khuynh đảo phòng vé Việt
                    </h4>
                    <p className="text-secondary">
                      Bom tấn Marvel "Spider-Man: No way home" thu về 24 tỷ đồng
                      trong nước sau ba ngày chiếu.
                    </p>
                  </div>
                </a>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  href="https://vnexpress.net/spider-man-3-nhan-mua-loi-khen-4403317.html"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://i1-giaitri.vnecdn.net/2021/12/15/Spiderman3-1639537070-5124-1639537177.jpg?w=300&h=180&q=100&dpr=1&fit=crop&s=SEQb4j5DKRMhy28lQ7vNmw"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      Spider-Man 3 nhận 'mưa' lời khen
                    </h4>
                    <p className="text-secondary">
                      Bom tấn "Spider-Man: No Way Home" nhận 97% đánh giá tích
                      cực trên Rotten Tomatoes từ giới phê bình sau các buổi
                      chiếu sớm.
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  href="https://vnexpress.net/tang-qua-phim-a-quiet-place-2-4402068.html"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://i1-giaitri.vnecdn.net/2021/12/12/AquietPlace-1639302244-1263-1639302293.jpg?w=300&h=180&q=100&dpr=1&fit=crop&s=lAxFlyk4L3-Jxys2ar-_0Q"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      Tặng quà phim 'A Quiet Place 2'
                    </h4>
                    <p className="text-secondary">
                      VnExpress tặng độc giả các phần quà từ phim kinh dị "A
                      Quiet Place 2" - có vợ chồng minh tinh Emily Blunt đóng
                      chính.
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7961-khai-truong-rap-xin-gia-ngon-chuan-xi-tai-sai-gon"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2021/01/khai-truong-rap-xin-gia-ngon-chuan-xi-tai-sai-gon-16115477671555.jpg"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        Khai trương rạp xịn giá ngon, chuẩn xì-tai Sài Gòn
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7960-boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/11/boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh-16056939435004.png"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        “Bóc tem” tổ hợp giải trí mới toanh của giới Hà Thành
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7957-tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/11/tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu-16043751284117.png"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần
                        công
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7956-ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/10/ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman-16041584850247.jpg"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        NGÔ THANH VÂN CHÍNH THỨC KHỞI ĐỘNG CUỘC THI THIẾT
                      </h6>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </TabPanel>
        </Fade>
        <Fade timeout={400} in={value === 1}>
          <TabPanel value={value} index={1}>
            <div className="row">
              <div className={classes.repons}>
                <a
                  href="https://www.galaxycine.vn/binh-luan-phim/review-spider-man-no-way-home-chac-chan-la-phim-nhen-xuat-sac-nhat-tu-truoc-den-nay"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://www.galaxycine.vn/media/2021/12/20/review-no-way-home-chac-chan-la-phim-nhen-xuat-sac-nhat-tu-truoc-den-nay-4_1639936843544.jpg"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      [Review] Spider-Man No Way Home: Chắc Chắn Là Phim Nhện
                      Xuất Sắc Nhất Từ Trước Đến Nay
                    </h4>
                    <p className="text-secondary">
                      Năm 2002, Sony ra mắt Spider-Man do Tobey Maguire đóng
                      chính. Tính đến nay, chỉ trong vòng mười chín năm đã có
                      tám phim Nhện công chiếu. Đây là một con số đáng mơ ước
                      không chỉ với siêu anh hùng mà còn là nhân vật phim ảnh.
                      Điều này chứng minh sức hút khó thể cưỡng của cậu hàng xóm
                      thân thiện Peter Parker suốt gần hai thập kỉ qua. Xem thêm
                      tại:
                    </p>
                  </div>
                </a>
              </div>
              <div className={classes.repons}>
                <a
                  href="https://tix.vn/review/7946-review-dinh-thu-oan-khuat-ghost-of-war"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://www.galaxycine.vn/media/2021/12/12/venom-let-there-be-carnage-tom-hardy-qua-xuat-sac-6_1639276287940.jpg"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      [Review] Venom Let There Be Carnage: Tom Hardy Quá Xuất
                      Sắc!
                    </h4>
                    <p className="text-secondary">
                      Năm 2018, Sony trình chiếu Venom. Ngày giới phê bình công
                      bố điểm, Cà chua thối khiến khán giả e ngại. Thế nhưng,
                      chỉ ít bữa sau, ai chịu tới rạp lại hết lời khen ngợi.
                      Điều đó thúc đẩy sự tò mò và dần giúp Venom được đón nhận
                      nhiều hơn.
                    </p>
                  </div>
                </a>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  href="https://www.galaxycine.vn/movie-blog/no-time-to-die-daniel-craig---tu-ke-bi-ghet-bo-toi-nguoi-hung"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://www.galaxycine.vn/media/2021/12/20/no-time-to-die-daniel-craig---tu-ke-bi-ghet-bo-toi-nguoi-hung-4_1639939712193.jpg"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      No Time To Die: Daniel Craig - Từ Kẻ Bị Ghét Bỏ Tới Người
                      Hùng!
                    </h4>
                    <p className="text-secondary">
                      Nhân vật siêu điệp viên James Bond kể từ khi được đưa lên
                      màn ảnh đã có lịch sử dài gắn bó cùng các tài tử. Một
                      trong những cái tên sáng giá mà khi nhắc đến anh, không
                      thể không nhắc tới đóng góp to lớn đối với thương hiệu là
                      Daniel Craig. Chặng đường 15 năm cống hiến đánh dấu bằng
                      hàng loạt tác phẩm thành công lẫy lừng
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  href="https://www.galaxycine.vn/movie-blog/he-lo-dan-long-tieng-cuc-khung-tu-the-boss-baby-family-business"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://www.galaxycine.vn/media/2021/12/20/he-lo-dan-long-tieng-cuc-khung-tu-the-boss-baby-family-business-6_1639938336025.jpg"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      Hé Lộ Dàn Lồng Tiếng Cực Khủng Từ The Boss Baby: Family
                      Business
                    </h4>
                    <p className="text-secondary">
                      Bên cạnh sự xuất hiện của tài tử Alec Baldwin trong vai
                      chính Nhóc Trùm, phim mới The Boss Baby: Family Business
                      còn gây chú ý nhờ quy tụ dàn sao nam lồng tiếng cực khủng.
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7961-khai-truong-rap-xin-gia-ngon-chuan-xi-tai-sai-gon"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2021/01/khai-truong-rap-xin-gia-ngon-chuan-xi-tai-sai-gon-16115477671555.jpg"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        Khai trương rạp xịn giá ngon, chuẩn xì-tai Sài Gòn
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7960-boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/11/boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh-16056939435004.png"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        “Bóc tem” tổ hợp giải trí mới toanh của giới Hà Thành
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7957-tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/11/tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu-16043751284117.png"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần
                        công
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7956-ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/10/ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman-16041584850247.jpg"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        NGÔ THANH VÂN CHÍNH THỨC KHỞI ĐỘNG CUỘC THI THIẾT
                      </h6>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </TabPanel>
        </Fade>
        <Fade timeout={400} in={value === 2}>
          <TabPanel value={value} index={2}>
            <div className="row">
              <div className={classes.repons}>
                <a
                  href="https://tix.vn/khuyen-mai/7958-bhd-59k-ve-ca-tuan"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://s3img.vcdn.vn/123phim/2021/04/bhd-59k-ve-ca-tuan-16190002421777.jpg"
                    alt="news-movie"
                  />

                  <div className="py-3">
                    <h4 className="card-title">BHD 59K/VÉ CẢ TUẦN !!!</h4>
                    <p className="text-secondary">
                      Tận hưởng Ưu Đãi lên đến 3 VÉ BHD Star mỗi tuần chỉ với
                      giá 59k/vé khi mua vé trên TIX hoặc Mục Vé Phim trên
                      ZaloPay.
                    </p>
                  </div>
                </a>
              </div>
              <div className={classes.repons}>
                <a
                  href="https://tix.vn/khuyen-mai/7955-tix-1k-ve-ngai-chi-gia-ve"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://s3img.vcdn.vn/123phim/2020/11/tix-1k-ve-ngai-chi-gia-ve-16045662877511.jpg"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">TIX 1K/VÉ NGẠI CHI GIÁ VÉ</h4>
                    <p className="text-secondary">
                      Đồng giá 1k/vé cả tuần tất cả các rạp trên TIX + Nhận thêm
                      02 voucher thanh toán ZaloPay thả ga
                    </p>
                  </div>
                </a>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  href="https://tix.vn/goc-dien-anh/7963-promising-young-woman-bong-hong-nuoc-anh-carey-mulligan-va-man-tra-thu-dan-ong-de-doi"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://s3img.vcdn.vn/123phim/2021/03/promising-young-woman-bong-hong-nuoc-anh-carey-mulligan-va-man-tra-thu-dan-ong-de-doi-16166710855522.png"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      PROMISING YOUNG WOMAN | Bông hồng nước Anh Carey Mulligan
                      và màn trả thù...
                    </h4>
                    <p className="text-secondary">
                      Đề cử giải Oscar danh giá vừa gọi tên Carey Mulligan ở
                      hạng mục nữ chính xuất sắc nhất cho vai diễn "đẫm máu"
                      nhất sự nghiệp của cô trong phim
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  href="https://tix.vn/goc-dien-anh/7962-vua-dep-lai-vua-tai-nang-dan-sao-nam-cua-phim-ban-tay-diet-quy-dam-bao-don-tim-hoi-chi-em"
                  className={classes.news}
                >
                  <img
                    className={classes.fullImg}
                    src="https://s3img.vcdn.vn/123phim/2021/03/vua-dep-lai-vua-tai-nang-dan-sao-nam-cua-phim-ban-tay-diet-quy-dam-bao-don-tim-hoi-chi-em-16165783843676.png"
                    alt="news-movie"
                  />
                  <div className="py-3">
                    <h4 className="card-title">
                      VỪA ĐẸP LẠI VỪA TÀI NĂNG, DÀN SAO NAM CỦA PHIM “BÀN TAY
                      DIỆT QUỶ”...
                    </h4>
                    <p className="text-secondary">
                      Quy tụ 3 nam tài tử vừa điển trai, vừa được đánh giá cao
                      về năng lực diễn xuất là Park Seo Joon, Woo Do Hwan và
                      Choi Woo Sik, tác phẩm kinh dị – hành
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-sm-4 pl-0 pr-15">
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7961-khai-truong-rap-xin-gia-ngon-chuan-xi-tai-sai-gon"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2021/01/khai-truong-rap-xin-gia-ngon-chuan-xi-tai-sai-gon-16115477671555.jpg"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        Khai trương rạp xịn giá ngon, chuẩn xì-tai Sài Gòn
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7960-boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/11/boc-tem-to-hop-giai-tri-moi-toanh-cua-gioi-ha-thanh-16056939435004.png"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        “Bóc tem” tổ hợp giải trí mới toanh của giới Hà Thành
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7957-tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/11/tiec-trang-mau-chinh-thuc-can-moc-100-ty-chi-sau-2-tuan-cong-chieu-16043751284117.png"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần
                        công
                      </h6>
                    </div>
                  </div>
                </a>
                <a
                  className={classes.bonusNews}
                  href="https://tix.vn/goc-dien-anh/7956-ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman"
                >
                  <div className="row mb-2">
                    <div className="col-3 px-0">
                      <img
                        className={classes.fullImg}
                        src="https://s3img.vcdn.vn/123phim/2020/10/ngo-thanh-van-chinh-thuc-khoi-dong-cuoc-thi-thiet-ke-trang-phuc-cho-sieu-anh-hung-dau-tien-cua-viet-nam-vinaman-16041584850247.jpg"
                        alt="news-movie"
                      />
                    </div>
                    <div className="col-9">
                      <h6 className="text-secondary">
                        NGÔ THANH VÂN CHÍNH THỨC KHỞI ĐỘNG CUỘC THI THIẾT
                      </h6>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </TabPanel>
        </Fade>
      </div>
    </div>
  );
}
