import React from "react";

import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import useStyles from "./style";
import RightSection from "./RightSection";

export default function LichChieuDesktop({ data }) {
  const classes = useStyles();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      {/* logo và tên cụm rạp bên trái */}
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        classes={{ root: classes.leftSection, indicator: classes.indicator }}
      >
        {data?.heThongRapChieu?.map((theater) => (
          <Tab
            disableRipple
            key={theater.maHeThongRap}
            classes={{ wrapper: classes.wrapper, root: classes.tabRoot }}
            label={
              <>
                <img
                  className={classes.logo}
                  src={theater.logo}
                  alt="logoTheater"
                />
                <span>{theater.tenHeThongRap}</span>
              </>
            }
          />
        ))}
      </Tabs>
      {/* End logo và tên cụm rạp bên trái */}

      {/* Hiển thị nội dung  bên phải  if esle  */}
      <div className={classes.rightSection}>
        {data?.heThongRapChieu?.length === 0 && (
          <p style={{ padding: 10 }}>
            Hiện tại chưa có lịch chiếu cho phim này
          </p>
        )}
        {data?.heThongRapChieu?.map((theater, i) => (
          <div
            key={theater.maHeThongRap}
            style={{ display: value === i ? "block" : "none" }}
          >
            {/* Hiển thị nội dung chi tiết  bên phải  */}
            <RightSection currentSelectedHeThongRapChieu={theater} />
          </div>
        ))}
      </div>
    </div>
  );
}
